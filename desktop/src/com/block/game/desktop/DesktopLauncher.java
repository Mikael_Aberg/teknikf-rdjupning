package com.block.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.block.game.main.Application;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.backgroundFPS = 60;
		config.foregroundFPS = 60;
		config.width = (int) (Application.V_WIDTH * Application.SCALE);
		config.height = (int) (Application.V_HEIGHT * Application.SCALE);
		config.resizable = false;
		new LwjglApplication(new Application(), config);
	}
}
