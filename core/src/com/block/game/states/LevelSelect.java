package com.block.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.block.game.handlers.GameButton;
import com.block.game.handlers.GameStateManager;
import com.block.game.handlers.ScrollBackground;
import com.block.game.main.Application;

import java.io.File;
import java.io.FilenameFilter;

public class LevelSelect extends GameState {

    private boolean debug = true;
    private GameButton[] levelButtons;
    private int numberOfLevels;
    private int levelUnlock;
    private ScrollBackground bg;
    private float startX;
    private float startY;
    private GameStateManager gsm;
    public static int page = 1;
    private int buttonStartLevel;
    private int numberOfPages;
    private GameButton leftButton, rightButton;
    private BitmapFont bmf;


    public LevelSelect(GameStateManager gsm) {
        super(gsm);
        this.gsm = gsm;
        numberOfLevels = 0;
        buttonStartLevel = (page * 6) - 5;
        bmf = new BitmapFont();

        Texture locked = Application.res.getTexture("locked_button");
        Texture redButton = Application.res.getTexture("lvl_button");

        Preferences pref = Gdx.app.getPreferences("myPreferences");
        levelUnlock = pref.getInteger("highLevel", 1);
        getAllLevels();

        Texture texture = Application.res.getTexture("button_right");
        rightButton = new GameButton(new TextureRegion(texture, 40, 40), 290, 25, this.cam);
        texture = Application.res.getTexture("button_left");
        leftButton = new GameButton(new TextureRegion(texture, 40, 40), 30, 25, this.cam);

        int numberOfButtons = 0;

        numberOfPages = (int) Math.ceil(numberOfLevels / 6.0);

        for (int i = buttonStartLevel; i <= numberOfLevels; i++) {
            numberOfButtons++;
            if (numberOfButtons >= 6) {
                numberOfButtons = 6;
                break;
            }
        }

        levelButtons = new GameButton[numberOfButtons];

        pref.putInteger("levels", numberOfLevels);
        pref.flush();

        if (debug) levelUnlock = numberOfLevels;

        Texture tex = Application.res.getTexture("menu");
        this.bg = new ScrollBackground(new TextureRegion(tex), this.cam, 1.0f);
        this.bg.setVector(-20.0F, 0.0F);

        startX = redButton.getWidth();

        startY = Application.V_HEIGHT / Application.SCALE - redButton.getHeight() / 2;

        int buttonRow = 3;
        int rowNr = 0;

        for (int i = 0; i < numberOfButtons; i++, rowNr++) {

            if (i == buttonRow) {
                buttonRow += 3;
                rowNr = 0;
                startY -= redButton.getHeight() + 10f;
                startX = redButton.getWidth();
            }

            if (buttonStartLevel <= levelUnlock && i + 1 <= numberOfLevels) {
                GameButton b = new GameButton(new TextureRegion(redButton, 80, 80), startX + redButton.getWidth() * rowNr, startY - 10f, this.cam);
                b.setLocked(false);
                b.setLevel(buttonStartLevel);
                b.setText(Integer.toString(buttonStartLevel));
                levelButtons[i] = b;
                buttonStartLevel++;

            } else if (buttonStartLevel > levelUnlock && i + 1 <= numberOfLevels) {
                GameButton b = new GameButton(new TextureRegion(locked, 80, 80), startX + redButton.getWidth() * rowNr, startY - 10f, this.cam);
                b.setLocked(true);
                levelButtons[i] = b;
                buttonStartLevel++;
            }

            if (rowNr >= 0) {
                startX += 10f;
            }

        }

    }

    public static void scrollScreen(int amount) {
        switch (amount) {
            case (-1):
                break;
            case (1):
                break;
            default:
                break;
        }
    }

    @Override
    public void handleInput() {

        for (GameButton b : levelButtons) {
            if (b.isClicked()) {
                if (!b.isLocked()) {
                    Play.level = b.getLevel();
                    gsm.setState(GameStateManager.PLAY);
                }
            }
        }

        if(rightButton.isClicked()){
            page++;
            gsm.setState(GameStateManager.LEVELSELECT);
        }
        if(leftButton.isClicked()){
            page--;
            gsm.setState(GameStateManager.LEVELSELECT);
        }
    }

    @Override
    public void update(float delta) {

        bg.update(delta);

        handleInput();
        if(page > 1) {
            leftButton.update(delta);
        }
        if(page < numberOfPages) {
            rightButton.update(delta);
        }
        for (GameButton b : levelButtons) {
            b.update(delta);
        }


    }

    @Override
    public void render() {

        sb.setProjectionMatrix(this.cam.combined);
        bg.render(this.sb);
        if(page > 1) {
            leftButton.render(sb);
        }
        if(page < numberOfPages) {
            rightButton.render(sb);
        }
        for (GameButton b : levelButtons) {
            b.render(sb);
        }

        if(numberOfPages > 1){
            sb.begin();
            bmf.draw(sb, page + " / " + numberOfPages,150, 20);
            sb.end();
        }


    }

    @Override
    public void dispose() {

    }

    private void getAllLevels() {

        //noinspection ResultOfMethodCallIgnored
        new File("maps").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (name.endsWith(".tmx")) {
                    numberOfLevels++;
                }
                return true;
            }
        });
    }
}
