package com.block.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.block.game.entities.Crystal;
import com.block.game.entities.HUD;
import com.block.game.entities.Player;
import com.block.game.handlers.*;
import com.block.game.main.Application;
import com.block.game.utils.BodyBuilder;

import static com.block.game.handlers.B2DVars.*;

public class Play extends GameState {

    public static int level;
    public static boolean coop;
    public static boolean countingDown;
    private boolean down;
    private boolean down2;
    private World world;
    private MyContactListener cl;
    private TiledMap tiledMap;
    private OrthogonalTiledMapRenderer tmr;
    private int mapwidth;
    private Box2DDebugRenderer b2dRenderer;
    private BoundedCamera b2dCam;
    private boolean debug = false;
    private float cameraWait = 0;
    private HUD hud;
    private HUD hud2;
    private Player player;
    private Player player2;
    private Array<Crystal> crystals;
    private Countdown countdown;

    public Play(GameStateManager gsm) {
        super(gsm);

        //Set box2d stuff
        world = new World(new Vector2(0, 0), true);
        down = true;
        down2 = true;
        cl = new MyContactListener();
        world.setContactListener(cl);

        countdown = new Countdown();

        b2dRenderer = new Box2DDebugRenderer();
        b2dCam = new BoundedCamera();
        b2dCam.setToOrtho(false, Application.V_WIDTH / PPM, Application.V_HEIGHT / PPM);

        //Load tile map
        try {
            tiledMap = new TmxMapLoader().load("maps/level" + level + ".tmx");
        } catch (Exception e) {
            System.out.println("Could not load level " + level);
            Gdx.app.exit();
        }

        tmr = new OrthogonalTiledMapRenderer(tiledMap);

        mapwidth = tiledMap.getProperties().get("width", Integer.class) * tiledMap.getProperties().get("tilewidth", Integer.class);

        //create player
        createPlayer();

        crystals = BodyBuilder.createCrystals(world, tiledMap.getLayers().get("crystals"));
        BodyBuilder.createWalls(world, tiledMap.getLayers().get("collision"));

        player.setTotalCrystals(crystals.size);

        if (coop) {
            player.setTotalCrystals(crystals.size / 2);
            player2.setTotalCrystals(crystals.size / 2);
            hud2 = new HUD(player2);
        }
        hud = new HUD(player);

        cam.position.set(cam.viewportWidth / 2, Application.V_HEIGHT / Application.SCALE / 2, 0);
        cam.update();

        countingDown = true;

    }

    @Override
    public void handleInput() {

        if ((Gdx.input.isKeyJustPressed(MyInput.BUTTON1) && cl.isPlayer1OnGround())) {
            down = !down;
            player.flipPlayer();
        }

        if (((coop) && Gdx.input.isKeyJustPressed(MyInput.BUTTON2) && cl.isPlayer2OnGround())) {
            down2 = !down2;
            player2.flipPlayer();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            gsm.setState(GameStateManager.MENU);
        }

        if (!countingDown) {
            if (coop && down2) {
                player2.getBody().setLinearVelocity(new Vector2(2.0f, -4.0f));
            } else if (coop && !down2) {
                player2.getBody().setLinearVelocity(new Vector2(2.0f, 4.0f));
            }

            if (down) {
                player.getBody().setLinearVelocity(new Vector2(2.0f, -4.0f));
            } else {
                player.getBody().setLinearVelocity(new Vector2(2.0f, 4.0f));
            }
        }
    }

    private void cameraUpdate(float delta) {
        if (!countingDown) {
            cameraWait += delta;
            if (cameraWait > 0.5f && cam.position.x + cam.viewportWidth / 2 < mapwidth) {
                cam.position.set((cam.position.x += BoundedCamera.CAMSPEED * delta), Application.V_HEIGHT / Application.SCALE / 2, 0);
                cam.update();
            }

            if (cam.position.x + cam.viewportWidth / 2 >= mapwidth) {
                cam.position.set(mapwidth - cam.viewportWidth / 2, Application.V_HEIGHT / Application.SCALE / 2, 0);
                cam.update();
            }
        }
    }

    @Override
    public void update(float delta) {

        if (player.getBody().getPosition().y > Application.V_HEIGHT / PPM || player.getBody().getPosition().y < 0) {
            lose();
        }

        if (player.getBody().getPosition().x * PPM < cam.position.x - cam.viewportWidth / 2) {
            lose();
        }

        if (player.getBody().getPosition().x > mapwidth / PPM) {
            win();
        }

        if (coop) {
            if (player2.getBody().getPosition().y > Application.V_HEIGHT / PPM || player.getBody().getPosition().y < 0) {
                lose();
            }

            if (player2.getBody().getPosition().x * PPM < cam.position.x - cam.viewportWidth / 2) {
                lose();
            }

            if (player2.getBody().getPosition().x > mapwidth / PPM) {
                win();
            }
        }

        countdown.update(delta);

        handleInput();

        //remove crystals
        Array<Body> bodies = cl.getBodiesToRemove();

        for (int i = 0; i < bodies.size; i++) {
            Body b = bodies.get(i);

            if (b.getFixtureList().get(0).getUserData().equals("crystal1")) {
                player.collectCrystal();
            } else if (b.getFixtureList().get(0).getUserData().equals("crystal2")) {
                player2.collectCrystal();
            }
            crystals.removeValue((Crystal) b.getUserData(), true);
            world.destroyBody(b);
        }

        bodies.clear();

        if (!countingDown) player.update(delta);

        if (coop && !countingDown) player2.update(delta);

        cameraUpdate(delta);

        for (int i = 0; i < crystals.size; i++) {
            crystals.get(i).update(delta);
        }
        world.step(delta, 1, 1);
    }

    @Override
    public void render() {

        //Clear screen
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (coop) {

            Gdx.gl20.glViewport(0, Application.V_HEIGHT, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2);

            //draw tile map
            tmr.setView(cam);
            tmr.render();

            //draw Player
            sb.setProjectionMatrix(cam.combined);
            player.render(sb);

            //Draw crystals
            for (int i = 0; i < crystals.size; i++) {
                Crystal b = crystals.get(i);
                if (b.getBody().getFixtureList().get(0).getUserData().equals("crystal1")) {
                    crystals.get(i).render(sb);
                }
            }

            sb.setProjectionMatrix(hudCam.combined);
            hud.render(sb);

            Gdx.gl20.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2);

            //draw tile map
            tmr.setView(cam);
            tmr.render();

            //draw Player
            sb.setProjectionMatrix(cam.combined);
            player2.render(sb);

            //Draw crystals
            for (int i = 0; i < crystals.size; i++) {
                Crystal b = crystals.get(i);
                if (b.getBody().getFixtureList().get(0).getUserData().equals("crystal2")) {
                    crystals.get(i).render(sb);
                }
            }

            sb.setProjectionMatrix(hudCam.combined);
            hud2.render(sb);

            if (debug) {
                b2dCam.setPosition(player2.getPosition().x + Application.V_WIDTH / 4 / PPM, Application.V_HEIGHT / 2 / PPM);
                b2dCam.update();
                b2dRenderer.render(world, b2dCam.combined);
            }


        } else {

            Gdx.gl20.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

            //draw tile map
            tmr.setView(cam);
            tmr.render();

            //draw Player
            sb.setProjectionMatrix(cam.combined);
            player.render(sb);

            //Draw crystals
            for (int i = 0; i < crystals.size; i++) {
                crystals.get(i).render(sb);
            }

            sb.setProjectionMatrix(hudCam.combined);
            hud.render(sb);

            if (debug) {
                b2dCam.setPosition(player.getPosition().x + Application.V_WIDTH / 4 / PPM, Application.V_HEIGHT / 2 / PPM);
                b2dCam.update();
                b2dRenderer.render(world, b2dCam.combined);
            }
        }

        if (countingDown) countdown.render(sb);



        if (coop && countingDown) {
            Gdx.gl20.glViewport(0, Application.V_HEIGHT, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2);
            countdown.render(sb);
        }
    }

    private void win() {
        System.out.println("Win!");
        level++;
        Preferences pref = Gdx.app.getPreferences("myPreferences");

        if(pref.getInteger("highLevel") < level){
            pref.putInteger("highLevel", level);
            pref.flush();
        }

        if (pref.getInteger("levels") < level) {
            gsm.setState(GameStateManager.MENU);
        } else {
            gsm.setState(GameStateManager.PLAY);
        }
    }

    private void lose() {
        System.out.println("Fail");
        gsm.setState(GameStateManager.PLAY);
    }

    @Override
    public void dispose() {
        Array<Body> bodies = new Array<Body>();
        world.getBodies(bodies);

        for(int i = 0; i < bodies.size; i++){
            world.destroyBody(bodies.get(i));
        }

        tiledMap.dispose();
        b2dRenderer.dispose();
    }

    private void createPlayer() {

        Body pBody;

        BodyDef def = new BodyDef();
        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();

        MapObject start = tiledMap.getLayers().get("start").getObjects().get(0);

        def.position.set(start.getProperties().get("x", float.class) / PPM, start.getProperties().get("y", float.class) / PPM);
        def.type = BodyDef.BodyType.DynamicBody;

        pBody = world.createBody(def);

        shape.setAsBox(13 / PPM, 13 / PPM);

        fdef.shape = shape;
        fdef.filter.categoryBits = BIT_PLAYER;
        fdef.filter.maskBits = BIT_GROUND | BIT_CRYSTAL_1;
        pBody.createFixture(fdef).setUserData("player");

        //create foot sensor
        shape.setAsBox(12 / PPM, 2 / PPM, new Vector2(0, -13 / PPM), 0);
        fdef = new FixtureDef();
        fdef.shape = shape;
        fdef.isSensor = true;
        fdef.friction = 0;
        fdef.filter.categoryBits = BIT_PLAYER_SENSOR;
        fdef.filter.maskBits = BIT_GROUND;
        pBody.createFixture(fdef).setUserData("playerSensor");
        shape.setAsBox(12 / PPM, 2 / PPM, new Vector2(0, 13 / PPM), 0);
        pBody.createFixture(fdef).setUserData("playerSensor");
        player = new Player(pBody, false);
        pBody.setUserData(player);

        if (coop) {

            Body p2Body;
            def = new BodyDef();
            fdef = new FixtureDef();
            shape = new PolygonShape();

            fdef.isSensor = false;
            def.position.set(start.getProperties().get("x", float.class) / PPM, start.getProperties().get("y", float.class) / PPM);
            def.type = BodyDef.BodyType.DynamicBody;

            p2Body = world.createBody(def);

            shape.setAsBox(13 / PPM, 13 / PPM);

            fdef.shape = shape;
            fdef.filter.categoryBits = BIT_PLAYER;
            fdef.filter.maskBits = BIT_GROUND | BIT_CRYSTAL_2;
            p2Body.createFixture(fdef).setUserData("player");

            //create foot sensor
            shape.setAsBox(12 / PPM, 2 / PPM, new Vector2(0, -13 / PPM), 0);
            fdef = new FixtureDef();
            fdef.shape = shape;
            fdef.isSensor = true;
            fdef.friction = 0;
            fdef.filter.categoryBits = BIT_PLAYER_SENSOR;
            fdef.filter.maskBits = BIT_GROUND;
            p2Body.createFixture(fdef).setUserData("player2Sensor");
            shape.setAsBox(12 / PPM, 2 / PPM, new Vector2(0, 13 / PPM), 0);
            p2Body.createFixture(fdef).setUserData("player2Sensor");
            player2 = new Player(p2Body, true);
            p2Body.setUserData(player2);

        }
        shape.dispose();
    }
}
