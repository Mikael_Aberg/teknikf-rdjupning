package com.block.game.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.block.game.handlers.GameStateManager;
import com.block.game.main.Application;

public abstract class GameState {

    protected GameStateManager gsm;
    protected Application app;

    protected SpriteBatch sb;
    protected OrthographicCamera cam;
    protected OrthographicCamera hudCam;

    protected GameState(GameStateManager gsm) {
        this.gsm = gsm;
        app = gsm.app();
        sb = app.getSb();
        cam = app.getCam();
        hudCam = app.getHudCam();
    }

    public abstract void handleInput();

    public abstract void update(float dt);

    public abstract void render();

    public abstract void dispose();
}
