package com.block.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.block.game.handlers.Animation;
import com.block.game.handlers.Background;
import com.block.game.handlers.GameButton;
import com.block.game.handlers.GameStateManager;
import com.block.game.main.Application;

public class Menu extends GameState {

    private boolean debug = true;
    private Background bg;
    private Animation animation;
    private GameButton playButton, play2Button, exitButton;
    private World world;
    private Box2DDebugRenderer b2dRenderer;

    public Menu(GameStateManager gsm) {
        super(gsm);
        Gdx.gl20.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Texture tex = Application.res.getTexture("menu");
        this.bg = new Background(new TextureRegion(tex), this.cam, 1.0F);
        this.bg.setVector(-20.0F, 0.0F);
        tex = Application.res.getTexture("p1_walk");
        TextureRegion[] reg = new TextureRegion[4];

        for (int i = 0; i < reg.length; ++i) {
            reg[i] = new TextureRegion(tex, i * 32, 0, 32, 32);
        }

        this.animation = new Animation(reg, 0.083333336F);
        tex = Application.res.getTexture("play");
        this.playButton = new GameButton(new TextureRegion(tex, 80, 80), 100.0F, 190f, this.cam);
        tex = Application.res.getTexture("play2");
        this.play2Button = new GameButton(new TextureRegion(tex, 80, 80), 220.0f, 190f, this.cam);
        tex = Application.res.getTexture("exit");
        this.exitButton = new GameButton(new TextureRegion(tex, 80, 80), 160.0f, 100.0f, this.cam);

        this.cam.setToOrtho(false, 320.0F, 240.0F);
        this.world = new World(new Vector2(0.0F, -49.0F), true);
        this.b2dRenderer = new Box2DDebugRenderer();
        this.createTitleBodies();
    }

    private void createTitleBodies() {
    }

    public void handleInput() {
        if (this.playButton.isClicked()) {
            Play.coop = false;
            LevelSelect.page = 1;
            this.gsm.setState(GameStateManager.LEVELSELECT);
        }
        if (this.play2Button.isClicked()) {
            Play.coop = true;
            LevelSelect.page = 1;
            this.gsm.setState(GameStateManager.LEVELSELECT);
        }
        if (this.exitButton.isClicked()) {
            Gdx.app.exit();
        }

    }

    public void update(float dt) {
        this.handleInput();
        this.world.step(dt / 5.0F, 8, 3);
        this.bg.update(dt);
        this.animation.update(dt);
        this.playButton.update(dt);
        this.exitButton.update(dt);
        this.play2Button.update(dt);
    }


    public void render() {
        this.sb.setProjectionMatrix(this.cam.combined);
        this.bg.render(this.sb);
        this.playButton.render(this.sb);
        this.play2Button.render(this.sb);
        this.exitButton.render(this.sb);
        this.sb.begin();
        this.sb.draw(this.animation.getFrame(), 146.0F, 31.0F);
        this.sb.end();

        if (this.debug) {
            this.cam.setToOrtho(false, 3.2F, 2.4F);
            this.b2dRenderer.render(this.world, this.cam.combined);
            this.cam.setToOrtho(false, 320.0F, 240.0F);
        }
    }

    public void dispose() {
    }
}

