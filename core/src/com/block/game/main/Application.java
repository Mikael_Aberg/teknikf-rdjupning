package com.block.game.main;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.block.game.handlers.*;

public class Application implements ApplicationListener {

    public static final String TITLE = "Gravity Run";
    public static final float VERSION = 0.3f;
    public static final int V_WIDTH = 720;
    public static final int V_HEIGHT = 480;
    public static final float SCALE = 2.0f;
    public static final float STEP = 1 / 60f;

    public static Content res;
    private float accum;
    private boolean debug = false;
    private SpriteBatch sb;
    private BoundedCamera cam;
    private OrthographicCamera hudCam;
    private GameStateManager gsm;

    @Override
    public void create() {

        if (debug) startWithoutSaves();

        Gdx.input.setInputProcessor(new MyInputProcessor());

        res = new Content();
        res.loadTexture("images/p1_walk.png", "p1_walk");
        res.loadTexture("images/p1_walk_flipped.png", "p1_walk_flipped");
        res.loadTexture("images/p2_walk.png", "p2_walk");
        res.loadTexture("images/p2_walk_flipped.png", "p2_walk_flipped");
        res.loadTexture("images/crystal.png", "crystal");
        res.loadTexture("images/hud.png", "hud");
        res.loadTexture("images/menu.png", "menu");
        res.loadTexture("images/button_play.png", "play");
        res.loadTexture("images/lvl_button.png", "lvl_button");
        res.loadTexture("images/locked.png", "locked_button");
        res.loadTexture("images/Numbers16x18.png", "numbers");
        res.loadTexture("images/button_exit.png", "exit");
        res.loadTexture("images/button_play_2.png", "play2");
        res.loadTexture("images/button_right.png", "button_right");
        res.loadTexture("images/button_left.png", "button_left");

        sb = new SpriteBatch();

        cam = new BoundedCamera();
        cam.setToOrtho(false, V_WIDTH / SCALE, V_HEIGHT / SCALE);
        hudCam = new OrthographicCamera();
        hudCam.setToOrtho(false, V_WIDTH / SCALE, V_HEIGHT / SCALE);

        gsm = new GameStateManager(this);
    }

    private void startWithoutSaves() {
        Preferences pref = Gdx.app.getPreferences("myPreferences");
        pref.remove("highLevel");
        pref.remove("levels");
        pref.remove("button1");
        pref.remove("button2");
        pref.flush();
    }

    @Override
    public void resize(int width, int height) {
        cam.setToOrtho(false, width / SCALE, height / SCALE);
        hudCam.setToOrtho(false, width / SCALE, height / SCALE);
    }

    @Override
    public void render() {
        accum += Gdx.graphics.getDeltaTime();
        while (accum >= STEP) {
            Gdx.graphics.setTitle(TITLE + " v: " + VERSION + " FPS: " + Gdx.graphics.getFramesPerSecond());
            accum -= STEP;
            gsm.update(STEP);
            gsm.render();
            MyInput.update();
        }
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

    public SpriteBatch getSb() {
        return sb;
    }

    public OrthographicCamera getCam() {
        return cam;
    }

    public OrthographicCamera getHudCam() {
        return hudCam;
    }

}
