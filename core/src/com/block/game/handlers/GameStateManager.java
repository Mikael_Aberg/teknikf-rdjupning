package com.block.game.handlers;

import com.block.game.main.Application;
import com.block.game.states.*;

import java.util.Stack;

/**
 * Created by Mikael on 2015-12-02.
 */
public class GameStateManager {

    public static final int PLAY = 7659366;
    public static final int MENU = 4763468;
    public static final int LEVELSELECT = 9238732;
    private Application app;
    private Stack<GameState> gameStates;

    public GameStateManager(Application app) {
        this.app = app;
        gameStates = new Stack<GameState>();
        pushState(MENU);
    }

    public void update(float delta) {
        gameStates.peek().update(delta);

    }

    public void render() {
        gameStates.peek().render();
    }

    private GameState getState(int state) {

        switch (state){
            case (PLAY):
                return new Play(this);
            case (MENU):
                return new Menu(this);
            case (LEVELSELECT):
                return new LevelSelect(this);
        }

        return null;
    }

    public void setState(int state) {
        popState();
        pushState(state);
    }

    public void pushState(int state) {
        gameStates.push(getState(state));
    }

    public void popState() {
        GameState g = gameStates.pop();
        g.dispose();
    }

    public Application app() {
        return app;
    }

}
