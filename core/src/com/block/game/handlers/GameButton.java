package com.block.game.handlers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.block.game.main.Application;

public class GameButton {

    Vector3 vec;
    private float x;
    private float y;
    private float width;
    private float height;
    private TextureRegion reg;
    private OrthographicCamera cam;
    private boolean clicked;
    private String text;
    private TextureRegion[] font;
    private boolean locked;
    private int level;

    public GameButton(TextureRegion reg, float x, float y, OrthographicCamera cam) {
        locked = true;
        this.reg = reg;
        this.x = x;
        this.y = y;
        this.cam = cam;
        this.width = (float) reg.getRegionWidth();
        this.height = (float) reg.getRegionHeight();
        this.vec = new Vector3();
        Texture tex = Application.res.getTexture("hud");
        this.font = new TextureRegion[11];

        int i;
        for (i = 0; i < 6; ++i) {
            this.font[i] = new TextureRegion(tex, 32 + i * 9, 16, 9, 9);
        }

        for (i = 0; i < 5; ++i) {
            this.font[i + 6] = new TextureRegion(tex, 32 + i * 9, 25, 9, 9);
        }
    }

    public boolean isClicked() {
        return this.clicked;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getText() {
        return this.text;
    }

    public void setLevel(int level){
        this.level = level;
    }

    public int getLevel(){return level;}

    public void setText(String s) {
        this.text = s;
    }

    public void update(float dt) {
        this.vec.set((float) MyInput.x, (float) MyInput.y, 0.0F);
        this.cam.unproject(this.vec);
        if (MyInput.isPressed() && this.vec.x > this.x - this.width / 2.0F && this.vec.x < this.x + this.width / 2.0F && this.vec.y > this.y - this.height / 2.0F && this.vec.y < this.y + this.height / 2.0F) {
            this.clicked = true;
        } else {
            this.clicked = false;
        }
    }

    public void render(SpriteBatch sb) {
        sb.begin();
        sb.draw(this.reg, this.x - this.width / 2.0F, this.y - this.height / 2.0F);
        if (this.text != null) {
            this.drawString(sb, this.text, this.x, this.y);
        }
        sb.end();
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    private void drawString(SpriteBatch sb, String s, float x, float y) {
        int len = s.length();
        float xo = (float) (len * this.font[0].getRegionWidth() / 2);
        float yo = (float) (this.font[0].getRegionHeight() / 2);

        for (int i = 0; i < len; ++i) {
            char c = s.charAt(i);
            if (c == 47) {
                c = 10;
            } else {
                if (c < 48 || c > 57) {
                    continue;
                }
                c = (char) (c - 48);
            }
            sb.draw(this.font[c], x + (float) (i * 9) - xo, y - yo);
        }

    }
}
