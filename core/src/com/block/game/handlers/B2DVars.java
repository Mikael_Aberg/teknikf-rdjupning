package com.block.game.handlers;

public class B2DVars {

    //Category bits
    public static final short BIT_PLAYER_SENSOR = 2;
    public static final short BIT_PLAYER = 16;
    public static final short BIT_CRYSTAL_1 = 32;
    public static final short BIT_GROUND = 4;
    public static final short BIT_CRYSTAL_2 = 8;
    //Pixels per meter
    public static float PPM = 100;


}
