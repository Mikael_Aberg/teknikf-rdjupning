package com.block.game.handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Preferences;
import com.block.game.states.LevelSelect;

public class MyInputProcessor extends InputAdapter {

    public MyInputProcessor() {

        Preferences pref = Gdx.app.getPreferences("MyPreferences");

        MyInput.BUTTON1 = pref.getInteger("button1", Input.Keys.Z);
        MyInput.BUTTON2 = pref.getInteger("button2", Input.Keys.M);
    }

    @Override
    public boolean scrolled(int amount) {
        LevelSelect.scrollScreen(amount);
        return true;
    }

    public boolean mouseMoved(int x, int y) {
        MyInput.x = x;
        MyInput.y = y;
        return true;
    }

    public boolean touchDragged(int x, int y, int pointer) {
        MyInput.x = x;
        MyInput.y = y;
        MyInput.down = true;
        return true;
    }

    public boolean touchDown(int x, int y, int pointer, int button) {
        MyInput.x = x;
        MyInput.y = y;
        MyInput.down = true;
        return true;
    }

    public boolean touchUp(int x, int y, int pointer, int button) {
        MyInput.x = x;
        MyInput.y = y;
        MyInput.down = false;
        return true;
    }
}
