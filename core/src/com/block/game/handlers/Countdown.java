package com.block.game.handlers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.block.game.main.Application;
import com.block.game.states.Play;

public class Countdown {

    private float wait;
    private Texture tex;
    private int countdown;

    public Countdown() {
        wait = 0;
        countdown = 48;
        tex = Application.res.getTexture("numbers");
    }

    public void update(float delta) {
        wait += delta;
    }

    public void render(SpriteBatch sb) {

        sb.begin();
        sb.draw(new TextureRegion(tex, countdown, 0, 16, 18), Application.V_WIDTH / 2, Application.V_HEIGHT / 2);
        sb.end();

        if (wait > 1f) {
            wait -= 1f;
            countdown -= 16;
        }

        if (countdown == 0) {
            Play.countingDown = false;
        }
    }
}
