package com.block.game.handlers;

public class MyInput {

    private static final int NUM_KEYS = 2;
    public static int x;
    public static int y;
    public static boolean down;
    public static boolean pdown;
    public static boolean[] keys = new boolean[2];
    public static boolean[] pkeys = new boolean[2];
    public static int BUTTON1 = 0;
    public static int BUTTON2 = 1;

    public MyInput() {
    }

    public static void update() {
        pdown = down;

        for (int i = 0; i < NUM_KEYS; ++i) {
            pkeys[i] = keys[i];
        }
    }

    public static boolean isPressed() {
        return down && !pdown;
    }

}
