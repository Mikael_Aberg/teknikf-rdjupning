package com.block.game.handlers;

import com.badlogic.gdx.graphics.OrthographicCamera;


public class BoundedCamera extends OrthographicCamera {
    public static float CAMSPEED = 200f;

    private float xmin;
    private float xmax;
    private float ymin;
    private float ymax;

    public BoundedCamera() {
        this(0.0F, 0.0F, 0.0F, 0.0F);
    }

    public BoundedCamera(float xmin, float xmax, float ymin, float ymax) {
        this.setBounds(xmin, xmax, ymin, ymax);
    }

    public void setBounds(float xmin, float xmax, float ymin, float ymax) {
        this.xmin = xmin;
        this.xmax = xmax;
        this.ymin = ymin;
        this.ymax = ymax;
    }

    public void setPosition(float x, float y) {
        this.setPosition(x, y, 0.0F);
    }

    public void setPosition(float x, float y, float z) {
        this.position.set(x, y, z);
        this.fixBounds();
    }

    private void fixBounds() {
        if (this.position.x < this.xmin + this.viewportWidth / 2.0F) {
            this.position.x = this.xmin + this.viewportWidth / 2.0F;
        }

        if (this.position.x > this.xmax - this.viewportWidth / 2.0F) {
            this.position.x = this.xmax - this.viewportWidth / 2.0F;
        }

        if (this.position.y < this.ymin + this.viewportHeight / 2.0F) {
            this.position.y = this.ymin + this.viewportHeight / 2.0F;
        }

        if (this.position.y > this.ymax - this.viewportHeight / 2.0F) {
            this.position.y = this.ymax - this.viewportHeight / 2.0F;
        }

    }
}

