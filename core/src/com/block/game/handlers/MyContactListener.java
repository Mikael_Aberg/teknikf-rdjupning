package com.block.game.handlers;

import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.block.game.states.Play;

public class MyContactListener implements ContactListener {

    private int numOfContacts, numOfContacts2;
    private Array<Body> bodiesToRemove;

    public MyContactListener() {
        super();
        bodiesToRemove = new Array<Body>();
    }

    // Called when two fixtures start to collide
    @Override
    public void beginContact(Contact c) {

        Fixture fa = c.getFixtureA();
        Fixture fb = c.getFixtureB();

        if (fa == null || fb == null) return;

        if (fa.getUserData() != null && fa.getUserData().equals("playerSensor")) {
            numOfContacts++;
        }
        if (fb.getUserData() != null && fb.getUserData().equals("playerSensor")) {
            numOfContacts++;
        }

        if (Play.coop) {
            if (fb.getUserData() != null && fa.getUserData().equals("player2Sensor")) {
                numOfContacts2++;
            }
            if (fa.getUserData() != null && fb.getUserData().equals("player2Sensor")) {
                numOfContacts2++;
            }
        }

        if (fa.getUserData() != null && (fb.getUserData().equals("crystal1") || fb.getUserData().equals("crystal2"))) {
            bodiesToRemove.add(fb.getBody());
        }

        if (fb.getUserData() != null && (fa.getUserData().equals("crystal1") || fa.getUserData().equals("crystal2"))) {
            bodiesToRemove.add(fa.getBody());
        }
    }

    // Called when two fixtures no longer collide
    @Override
    public void endContact(Contact c) {

        Fixture fa = c.getFixtureA();
        Fixture fb = c.getFixtureB();

        if (fa == null || fb == null) return;

        if (fa.getUserData() != null && fa.getUserData().equals("playerSensor")) {
            numOfContacts--;
        }
        if (fb.getUserData() != null && fb.getUserData().equals("playerSensor")) {
            numOfContacts--;
        }

        if (Play.coop) {
            if (fb.getUserData() != null && fa.getUserData().equals("player2Sensor")) {
                numOfContacts2--;
            }
            if (fa.getUserData() != null && fb.getUserData().equals("player2Sensor")) {
                numOfContacts2--;
            }
        }
    }

    public boolean isPlayer1OnGround() {
        return numOfContacts > 0;
    }

    public boolean isPlayer2OnGround() {
        return numOfContacts2 > 0;
    }

    public Array<Body> getBodiesToRemove() {
        return bodiesToRemove;
    }

    @Override
    public void preSolve(Contact c, Manifold m) {
    }

    @Override
    public void postSolve(Contact c, ContactImpulse ci) {
    }
}
