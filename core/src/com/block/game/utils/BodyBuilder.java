package com.block.game.utils;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.block.game.entities.Crystal;
import com.block.game.handlers.B2DVars;
import com.block.game.states.Play;

import static com.block.game.handlers.B2DVars.*;

/**
 * Created by Mikael on 2015-11-28.
 */
public class BodyBuilder {

    public static Body createCircle(World world, int x, int y, int radius, boolean isStatic, boolean canRotate) {
        Body pBody;
        BodyDef def = new BodyDef();

        if (isStatic) {
            def.type = BodyDef.BodyType.StaticBody;
        } else def.type = BodyDef.BodyType.DynamicBody;

        def.position.set(x / PPM, y / PPM);
        def.fixedRotation = !canRotate;
        pBody = world.createBody(def);

        CircleShape shape = new CircleShape();
        shape.setRadius(radius / PPM);

        FixtureDef fdef = new FixtureDef();
        fdef.shape = shape;
        shape.dispose();

        return pBody;
    }

    public static Body createBox(World world, float x, float y, float width, float height, boolean isStatic, boolean caRotate) {
        Body pBody;
        BodyDef def = new BodyDef();

        if (isStatic) {
            def.type = BodyDef.BodyType.StaticBody;
        } else def.type = BodyDef.BodyType.DynamicBody;

        def.position.set(x, y);
        def.fixedRotation = !caRotate;
        pBody = world.createBody(def);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height);

        FixtureDef fdef = new FixtureDef();
        fdef.shape = shape;
        shape.dispose();

        return pBody;
    }

    public static void loadTileMapBodies(World world, TiledMapTileLayer layer, short bits) {

        float tileSize = layer.getTileWidth();
        //row = y
        //col = x
        FixtureDef fdef = new FixtureDef();

        BodyDef bdef = new BodyDef();
        for (int row = 0; row < layer.getHeight(); row++) {
            for (int col = 0; col < layer.getWidth(); col++) {

                //get cell
                TiledMapTileLayer.Cell cell = layer.getCell(col, row);

                if (cell == null) continue;
                if (cell.getTile() == null) continue;


                bdef.type = BodyDef.BodyType.StaticBody;
                bdef.position.set(
                        (col + 0.5f) * tileSize / PPM,
                        (row + 0.5f) * tileSize / PPM
                );

                ChainShape cs = new ChainShape();
                Vector2[] v = new Vector2[3];
                v[0] = new Vector2(
                        -tileSize / 2 / PPM, -tileSize / 2 / PPM);
                v[1] = new Vector2(
                        -tileSize / 2 / PPM, tileSize / 2 / PPM);
                v[2] = new Vector2(
                        tileSize / 2 / PPM, tileSize / 2 / PPM);

                cs.createChain(v);
                fdef.friction = 0;
                fdef.shape = cs;
                fdef.filter.categoryBits = bits;
                fdef.filter.maskBits = B2DVars.BIT_PLAYER;
                fdef.isSensor = false;
                world.createBody(bdef).createFixture(fdef);

            }
        }
    }

    public static Array<Crystal> createCrystals(World world, MapLayer layer) {

        Array<Crystal> crystals = new Array<Crystal>();

        BodyDef bdef = new BodyDef();
        FixtureDef fdef = new FixtureDef();

        for (MapObject mo : layer.getObjects()) {

            bdef.type = BodyDef.BodyType.StaticBody;

            float x = mo.getProperties().get("x", float.class) / PPM;
            float y = mo.getProperties().get("y", float.class) / PPM;

            bdef.position.set(x, y);

            CircleShape cshape = new CircleShape();
            cshape.setRadius(8 / PPM);

            fdef.shape = cshape;
            fdef.isSensor = true;
            fdef.filter.maskBits = BIT_PLAYER;
            fdef.filter.categoryBits = BIT_CRYSTAL_1;

            Body body = world.createBody(bdef);
            body.createFixture(fdef).setUserData("crystal1");

            Crystal c = new Crystal(body);
            crystals.add(c);

            body.setUserData(c);

            if (Play.coop) {

                fdef.filter.categoryBits = BIT_CRYSTAL_2;
                fdef.filter.maskBits = BIT_PLAYER;

                Body body2 = world.createBody(bdef);
                body2.createFixture(fdef).setUserData("crystal2");

                Crystal c2 = new Crystal(body2);
                crystals.add(c2);

                body2.setUserData(c2);
            }

        }
        return crystals;
    }

    public static void createWalls(World world, MapLayer layer) {

        for (MapObject mo : layer.getObjects()) {

            RectangleMapObject rectangle = (RectangleMapObject) mo;

            Shape shape = getRectangle(rectangle);

            BodyDef bd = new BodyDef();
            bd.type = BodyDef.BodyType.StaticBody;
            FixtureDef fdef = new FixtureDef();
            fdef.friction = 0;
            fdef.shape = shape;
            fdef.filter.categoryBits = BIT_GROUND;
            world.createBody(bd).createFixture(fdef).setUserData("ground");
            shape.dispose();
        }
    }

    private static PolygonShape getRectangle(RectangleMapObject rectangleObject) {
        Rectangle rectangle = rectangleObject.getRectangle();
        PolygonShape polygon = new PolygonShape();
        Vector2 size = new Vector2((rectangle.x + rectangle.width * 0.5f) / PPM,
                (rectangle.y + rectangle.height * 0.5f) / PPM);
        polygon.setAsBox(rectangle.width * 0.5f / PPM,
                rectangle.height * 0.5f / PPM,
                size,
                0.0f);
        return polygon;
    }
}

