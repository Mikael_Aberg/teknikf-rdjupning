package com.block.game.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.block.game.main.Application;

public class Player extends B2DSprite {

    private int numCrystals;
    private int totalCrystals;

    private TextureRegion[] walk_normal, walk_fliped;
    private boolean fliped;


    public Player(Body body, boolean player2) {
        super(body);

        Texture tex;
        if (!player2) {

            tex = Application.res.getTexture("p1_walk");
            walk_normal = TextureRegion.split(tex, 32, 32)[0];
            tex = Application.res.getTexture("p1_walk_flipped");
            walk_fliped = TextureRegion.split(tex, 32, 32)[0];

            fliped = false;
            setAnimation(walk_normal, 1 / 12f);

        } else {

            tex = Application.res.getTexture("p2_walk");
            walk_normal = TextureRegion.split(tex, 32, 32)[0];
            tex = Application.res.getTexture("p2_walk_flipped");
            walk_fliped = TextureRegion.split(tex, 32, 32)[0];

            fliped = false;
            setAnimation(walk_normal, 1 / 12f);
        }
    }

    public void flipPlayer() {

        if (fliped) {
            fliped = false;
            setAnimation(walk_normal, 1 / 12f);
        } else {
            fliped = true;
            setAnimation(walk_fliped, 1 / 12f);
        }
    }

    public void collectCrystal() {
        numCrystals++;
    }

    public int getNumCrystals() {
        return numCrystals;
    }

    public int getTotalCrystals() {
        return totalCrystals;
    }

    public void setTotalCrystals(int i) {
        totalCrystals = i;
    }
}
