package com.block.game.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.block.game.main.Application;

public class HUD {

    private Player player;
    private TextureRegion crystal;
    private TextureRegion[] font;

    public HUD(Player player) {

        this.player = player;
        Texture tex = Application.res.getTexture("hud");

        this.crystal = new TextureRegion(tex, 80, 0, 16, 16);
        this.font = new TextureRegion[11];

        for (int i = 0; i < 6; ++i) {
            this.font[i] = new TextureRegion(tex, 32 + i * 9, 16, 9, 9);
        }

        for (int i = 0; i < 5; ++i) {
            this.font[i + 6] = new TextureRegion(tex, 32 + i * 9, 25, 9, 9);
        }
    }

    public void render(SpriteBatch sb) {
        sb.begin();
        sb.draw(this.crystal, crystal.getRegionWidth(), Application.V_HEIGHT - crystal.getRegionHeight() * 2);
        this.drawString(sb, this.player.getNumCrystals() + " / " + this.player.getTotalCrystals(), crystal.getRegionWidth() * 2.5f, Application.V_HEIGHT - crystal.getRegionHeight() * 1.8f);
        sb.end();
    }

    private void drawString(SpriteBatch sb, String s, float x, float y) {

        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == 47) {
                c = 10;
            } else {
                if (c < 48 || c > 57) {
                    continue;
                }

                c = (char) (c - 48);
            }
            sb.draw(this.font[c], x + (float) (i * 9), y);
        }

    }
}
